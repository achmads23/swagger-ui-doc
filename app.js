const express = require('express');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const app = express();
const port = 4000;

const swaggerDefinition = {
  openapi: '3.0.1',
  info: {
    title: 'REST API for my App', // Title of the documentation
    version: '1.0.0', // Version of the app
    description: 'This is the REST API for my product', // short description of the app
  },
  servers: [
    {
      "url": "http://localhost:8080",
      "description": "Local server"
    },
    {
      "url": "http://192.168.0.1:8080",
      "description": "Development server"
    }
  ],
};

// options for the swagger docs
const options = {
  // import swaggerDefinitions
  swaggerDefinition,
  // path to the API docs
  apis: ['./docs/**/*.yaml'],
};
// initialize swagger-jsdoc
const swaggerSpec = swaggerJSDoc(options);


// use swagger-Ui-express for your app documentation endpoint
app.use('/', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.listen(port, () => console.info(`Swagger UI YAML will be served from port ${port}!`));